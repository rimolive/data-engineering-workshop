# Workshop Abstract

As data is exponentially growing in organizations, there is an increasing need to consolidate silos of information into a single source of truth, a Data Lake to feed hungry Analytics and Machine Learning Engines that can gather insight at scale. In this workshop, we will detail how data engineers can process, manage and explore large-scale data for data science initiatives using open source industry-standard solutions running on OpenShift with the Open Data Hub project.

In this lab, attendees will learn how to:
- Store data in Ceph object storage
- Optimize storage of big data with compressed columnar file formats
- Catalog data sets using Hive Metastore and Hue
- Access and process cataloged data using Spark
- Create a data processing workflow with conditional steps using Argo
- Monitor the query performance of Spark using Prometheus