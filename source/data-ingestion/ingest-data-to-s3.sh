#!/bin/bash
set -e

# Check if s3cmd exists
if ! [ -x "$(command -v s3cmd)" ]; then
  echo 'Error: s3cmd is not installed.' >&2
  echo 'run 'pip install s3cmd' to install it.' >&2
  exit 1
fi

S3CMD_COMMAND=$(command -v s3cmd)

# Check if the environment variables are set
required_var() {
    VAR=$1
    if [[ -z ${!VAR} ]]; then
        echo "Error: environment variable $VAR is not set." >&2
        exit 1
    fi
}
required_var S3_ENDPOINT
required_var S3_ACCESS_KEY
required_var S3_SECRET_KEY

# Get the parameters
OCP_USER=$1
PATH=$2

$S3CMD_COMMAND \
    --configure \
    --no-ssl \
    --host=$S3_ENDPOINT \
    --access_key=$S3_ACCESS_KEY \
    --secret_key=$S3_SECRET_KEY \
    --region=US \
    --host-bucket=$S3_ENDPOINT

$S3CMD_COMMAND mb s3://workshop-$OCP_USER

$S3CMD_COMMAND put \
    $PATH \
    --recursive \
    --acl-public \
    --encoding=UTF-8 \
    s3://workshop-$OCP_USER/

echo "Data ingestion successful! Number of records uploaded:"
$S3CMD_COMMAND ls s3://workshop-$OCP_USER/hard-drive/ | wc -l